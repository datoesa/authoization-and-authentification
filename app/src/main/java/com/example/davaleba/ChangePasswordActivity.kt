package com.example.davaleba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class   ChangePasswordActivity : AppCompatActivity() {

   private lateinit var editTextNewPassword: EditText
   private lateinit var buttonNewPassword: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        init()

        registerListener()
    }

    private fun init() {
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        buttonNewPassword = findViewById(R.id.buttonNewPassword)
    }

    private fun registerListener() {
        buttonNewPassword.setOnClickListener {
            val newPassword = editTextNewPassword.text.toString()

            if(newPassword.isEmpty() || newPassword.length < 9) {
                Toast.makeText(this, "New Password should have at least 9 characters", Toast.LENGTH_LONG).show()
            } else {

                FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                    ?.addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Toast.makeText(this, "You're Password has been Successfully changed ", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }
}